﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class UIManager : MonoBehaviour
{
    public GameManager gameManager;
    public LevelManager levelManager;

    public GameObject startPanel;
    public GameObject stagePanel;
    public GameObject finalPanel;
    public GameObject failPanel;

    void Start()
    {
        ShowStartPanel();
    }


    public void ShowStartPanel()
    {
        if(Application.loadedLevel == 0)
        startPanel.SetActive(true);
    }


    public void ShowStagePanel()
    {
        stagePanel.SetActive(true);
    }


    public void ShowFinalPanel(int countStages, int countErrors)
    {
        finalPanel.GetComponent<UIFinalPanel>().LoadStatistics(countStages, countErrors);
        finalPanel.SetActive(true);
    }


    public void ShowFailPanel(List<UserError> errors)
    {
        failPanel.GetComponent<UIFailPanel>().LoadStatisticsError(errors);
        failPanel.SetActive(true);
    }


    public void UpdateStagePanel(Stage stage)
    {
        stagePanel.GetComponent<UIStagePanel>().Loadstage(stage);
    }


    public void CloseApp()
    {
        gameManager.CloseApp();
    }


    public void Reload()
    {
        failPanel.SetActive(false);
        ShowStartPanel();
        gameManager.Reload();
    }

    public void NextScene()
    {
        levelManager.NextScene();
    }


}
