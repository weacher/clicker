﻿using System.Collections;
using System.Collections.Generic;
using TMPro;
using UnityEngine;

public class UIFailPanel : UIComponent
{
    public TextMeshProUGUI LogError;

    public void LoadStatisticsError(List<UserError> errors)
    {
        LogError.text = "";
        for (int i = 0; i < errors.Count; i++)
        {
            LogError.text += " stage: " + errors[i].stageNumb + " task: " + errors[i].taskNumb + " " + errors[i].type.ToString()+ " " + "\n";
        }
        
    }
}
