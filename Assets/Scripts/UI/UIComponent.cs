﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class UIComponent : MonoBehaviour
{
    public void Close()
    {
        gameObject.SetActive(false);
    }
}
