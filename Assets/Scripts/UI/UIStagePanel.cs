﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine.UI;
using UnityEngine;
using TMPro;

public class UIStagePanel : UIComponent
{
    public TextMeshProUGUI NameStage;
    public TextMeshProUGUI DescriptionStage;
    public Transform TaskViewPort;

    public void Loadstage(Stage stage)
    {
        ClearTasks();

        NameStage.text = stage.nameStage;
        DescriptionStage.text = stage.descriptionStage;

        for (int i = 0; i < stage.tasks.Count; i++)
        {
            var Task = TaskViewPort.transform.GetChild(i);
            Task.gameObject.SetActive(true);
            Task.GetChild(0).GetComponent<TextMeshProUGUI>().text = stage.tasks[i].name;
            Task.GetChild(1).GetComponent<TextMeshProUGUI>().text = stage.tasks[i].description;
            Task.GetChild(2).GetComponent<Toggle>().isOn = stage.tasks[i].IsComplete;
        }
    }

    public void ClearTasks()
    {
        for (int i = 0; i < TaskViewPort.transform.childCount;i++)
        {
            var Task = TaskViewPort.transform.GetChild(i);
            Task.gameObject.SetActive(false);

        }
    }

}
