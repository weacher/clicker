﻿using System.Collections;
using System.Collections.Generic;
using TMPro;
using UnityEngine;

public class UIFinalPanel : UIComponent
{
    public TextMeshProUGUI countStage;
    public TextMeshProUGUI countError;

    public void LoadStatistics(int countStages, int countErrors)
    {
        if(countStages > 0)
            countStage.text = "Count Stages: " + countStages;
        else
            countStage.text = "Count Stages: " + 1;

        if (countErrors >= 0)
            countError.text = "Count Errors: " + countErrors;
        else
            countError.text = "Count Errors: " + 0;
    }


}
