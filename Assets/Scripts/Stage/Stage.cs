﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Stage : MonoBehaviour
{
    public List<TaskType> tasks = new List<TaskType>(); 
    public List<TaskType> complete_tasks = new List<TaskType>();
    public string nameStage { get; set; }
    public string descriptionStage { get; set; }
    public bool IsActive { get; set; }
    public bool IsComplete { get; set; }
    public int indexActiveTask = 0;
    public int maxCountTask = 9;


    public Stage(string nameStage,string descriptionStage, List<TaskType> tasks)
    {
        this.nameStage = nameStage;
        this.descriptionStage = descriptionStage;
        this.tasks = tasks;
    }


    public Stage(string descriptionStage, List<TaskType> tasks)
    {
        nameStage = "Stage";
        this.descriptionStage = descriptionStage;
        this.tasks = tasks;
    }


    public Stage (List<TaskType> tasks)
    {
        this.nameStage = "Stage";
        this.descriptionStage = "";
        this.tasks = tasks;
    }


    public Stage(string nameStage, string descriptionStage)
    {
        this.nameStage = nameStage;
        this.descriptionStage = descriptionStage;
        this.tasks = new List<TaskType>();
    }


    public Stage()
    {
        nameStage = "Stage";
        descriptionStage = "";
        this.tasks = new List<TaskType>();
    }


    public void addTask(TaskType task)
    {
        if (!IsComplete)
            tasks.Add(task);      
    }


    public bool deleteTask(int index)
    {
        if (index >= 0 && index < tasks.Count)
        {
            if (!tasks[index].IsActive)
            {
                if (index < indexActiveTask)
                {
                    tasks.RemoveAt(index);
                    indexActiveTask--;
                    return true;
                }
                if (index > indexActiveTask)
                {
                    tasks.RemoveAt(index);
                    return true;
                }
                return false;
            }
            else
            {
                if (index + 1 < tasks.Count)
                {
                    tasks[index].IsActive = false;
                    tasks[index + 1].setActive();
                    tasks.RemoveAt(index);
                    return true;
                }
                return false;
            }
        }

        return false;
    }


    public TaskType getActiveTasks()
    {
        if(tasks.Count > 0 && indexActiveTask < tasks.Count)
        {
            return tasks[indexActiveTask];
        }
        else
        {
            indexActiveTask = 0;
            return null;
        }
    }


    public TaskType getCompleteTasks()
    {
        return null;
    }


    public void disActual()
    {
        IsActive = false;
        if (tasks.Count > 0)
        {
            for (int i = 0; i < tasks.Count; i++)
            {
                tasks[i].IsActive = false;
            }
        }
    }


    public void clearTasks()
    {
        tasks.Clear();
    }


    public void SetActiveTask() 
    {
        tasks[indexActiveTask].setActive();
    }




}
