﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class StageManager : MonoBehaviour
{
    public ObjectsManager objectsManager;
    public GameManager gameManager;
    public List<Stage> stages = new List<Stage>();
    public int indexStageActual = 0;

    private void Start()
    {
        StartCoroutine(startLoadStages());
    }


    public void SetNextStage()
    {
        stages[indexStageActual].disActual();
        if (indexStageActual + 1 >= stages.Count)
        {
            gameManager.CompleteAll();
        }
        else
        {
            indexStageActual++;
            stages[indexStageActual].IsActive = true;
            if (stages[indexStageActual].tasks.Count>0)
                stages[indexStageActual].tasks[0].setActive();
        }
    }


    public void CompleteTask()
    {
        getActualTask().setComplete();
        if (stages[indexStageActual].indexActiveTask < stages[indexStageActual].tasks.Count - 1)
        {
            stages[indexStageActual].indexActiveTask++;
            stages[indexStageActual].tasks[stages[indexStageActual].indexActiveTask].setActive();
        }
        else
        {
            getActualTask().setComplete();
            SetNextStage();
        }     
    }


    public void SetActualStage()
    {
        disActualAll();
        if (indexStageActual <= stages.Count)
        {
            stages[indexStageActual].IsActive = true;
            stages[indexStageActual].SetActiveTask(); 
        }
    }


    public void AddTask(string name, string description, int form_id, Condition condition)
    {   if (stages[indexStageActual].tasks.Count < stages[indexStageActual].maxCountTask)
        {
            if (gameManager.objectsManager.ObjectsContains(form_id))
            {
                if (name == "" || description == "")
                    stages[indexStageActual].tasks.Add(new TaskType(name, description, gameManager.objectsManager.GetObject(form_id), condition));
                else
                    stages[indexStageActual].tasks.Add(new TaskType(gameManager.objectsManager.GetObject(form_id), condition));
            }
        }
        gameManager.UIUpdate();
    }


    public void AddStage(string name, string description, List<TaskType> tasks)
    {
        if (name == "" || description == "")
            stages.Add(new Stage(tasks));
        else
            stages.Add(new Stage(name, description, tasks));

        gameManager.UIUpdate();
    }


    public void AddStage(string name, string description)
    {
        if (name == "" || description == "")
            stages.Add(new Stage());
        else
            stages.Add(new Stage(name, description));

        gameManager.UIUpdate();
    }


    IEnumerator startLoadStages()
    {
        yield return new WaitForSeconds(0.05f);
        LoadStages();
        SetActualStage();
    }


    public Condition getActualCondition()
    {
        return stages[indexStageActual].getActiveTasks().condition;
    }


    public TaskType getActualTask()
    {
        return stages[indexStageActual].getActiveTasks();
    }


    public void disActualAll()
    {
        if (stages.Count > 0)
        {
            for (int i = 0; i < stages.Count; i++)
                stages[i].disActual();
        }
    }


    public void AddStage(Stage stage)
    {
        stages.Add(stage);
    }
    

    public void Clear()
    {
        disActualAll();
        stages.Clear();
    }

    public void Reload()
    {
        StartCoroutine(startLoadStages());
    }

    /// <summary>
    /// Неуспел доделать
    /// </summary>
    public void LoadStages()
    {
        switch (Application.loadedLevel)
        {
            case 0:
                AddStage(new Stage("STAGE 1", "stage 1 description", new List<TaskType>() {new TaskType(objectsManager.objects[0], new Click(1, true)),
                        new TaskType(objectsManager.objects[1], new Rotate(true)), new TaskType(objectsManager.objects[2], new Click(true)), }));
                AddStage(new Stage("STAGE 2", "stage 2 description", new List<TaskType>() { new TaskType(objectsManager.objects[1], new Click()) }));
                break;
            case 1:
                AddStage(new Stage("STAGE 1", "stage 1 description", new List<TaskType>() { new TaskType(objectsManager.objects[0], new Click(1, true)),
                    new TaskType(objectsManager.objects[3], new Rotate(true)), new TaskType(objectsManager.objects[2], new Rotate(true)),
                    new TaskType(objectsManager.objects[1], new Click(true)),new TaskType(objectsManager.objects[3], new Rotate(true)), }));
                AddStage(new Stage("STAGE 2", "stage 2 description", new List<TaskType>() { new TaskType(objectsManager.objects[5], new Rotate()), new TaskType(objectsManager.objects[4], new Rotate()) }));
                AddStage(new Stage("STAGE 3", "stage 3 description", new List<TaskType>() { new TaskType(objectsManager.objects[5], new Rotate()), new TaskType(objectsManager.objects[4], new Rotate()) }));
                break;
            case 2:
                AddStage(new Stage("STAGE 1", "stage 1 description", new List<TaskType>() { new TaskType(objectsManager.objects[2], new Rotate(true)), new TaskType(objectsManager.objects[1], new Rotate(true)), new TaskType(objectsManager.objects[2], new Rotate(true)), }));
                AddStage(new Stage("STAGE 2", "Next tasks", new List<TaskType>() { new TaskType(objectsManager.objects[2], new Rotate()) }));
                break;
        }
    }

    public void RemoveStage()
    {

    }

}
