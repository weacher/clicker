﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ClickActivity : Activity
{
    public ClickActivity(Form form)
    {
        this.form = form;
        this.activityType = activity_type.click;
        this.nameActivity = "click";
        this.descriptionActivity = "click on" + form.name;
    }
}
