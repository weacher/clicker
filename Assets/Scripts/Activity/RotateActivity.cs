﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class RotateActivity : Activity
{
    public Vector3 dicplacement { get; set; }
    public Vector3 angles { get; set; }

    public RotateActivity(Form form, params Vector3[] values)
    {
        this.form = form;
        this.dicplacement = dicplacement;
        this.nameActivity = "rotate";
        this.descriptionActivity = "rotate on" + form.name + "on " + dicplacement.x.ToString() + " " + dicplacement.y.ToString() + " " + dicplacement.z.ToString();
        this.dicplacement = values[0];
        this.angles= values[1];
        this.activityType = activity_type.rotate;

    }
}
