﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class CreatorClick : CreatorActivity
{
    public override Activity Create(Form form, params Vector3[] values)
    {
        return new ClickActivity(form);
    }
}
