﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public abstract class Activity : MonoBehaviour
{
   public activity_type activityType { get; set; }
   public string nameActivity { get; set; }
   public string descriptionActivity { get; set; }

   public Form form { get; set; }

}
