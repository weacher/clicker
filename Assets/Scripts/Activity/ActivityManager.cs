﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ActivityManager : MonoBehaviour
{
    public GameManager gameManager;
    CreatorClick creatorClick = new CreatorClick();

    CreatorRotate creatorRotate = new CreatorRotate();
    public List<Activity> activities = new List<Activity>();


    public void addActivity(Form form, activity_type type, params Vector3[] values)
    {
        switch(type){
            case activity_type.click:
                activities.Add(creatorClick.Create(form, null));
                break;
            case activity_type.rotate:
                activities.Add(creatorRotate.Create(form, values)); 
                break;
        }
        gameManager.Check(getLastActivity());      
    }


    public void addActivity(Activity activity)
    {
        activities.Add(activity);
        gameManager.Check(getLastActivity());
    }


    public Activity getLastActivity() 
    {
        if (activities.Count > 0)
        {
            return activities[activities.Count - 1];
        }
        else
        {
            return null;
        }         
    }


    public void Clear()
    {
        activities.Clear();
    }

}
