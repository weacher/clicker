﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public abstract class CreatorActivity : MonoBehaviour
{
    public abstract Activity Create(Form form, params Vector3[] values);
}
