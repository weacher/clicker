﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine.SceneManagement;
using UnityEngine;

public class LevelManager : MonoBehaviour
{
    public List<string> sceneList = new List<string>();  

    public void NextScene()
    {
        if(Application.loadedLevel + 1 < Application.levelCount)
        {
            Application.LoadLevel(Application.loadedLevel + 1);
        }

    }
}
