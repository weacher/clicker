﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;


public enum form_type
{
    cube,
    sphere,
    cylindre,
    capsule
}

public enum condition_type
{
    click,
    rotate
}

public enum activity_type
{
    click,
    rotate,
}

public enum error_type
{
    another_obj,
    another_condition,
    another_form,
}


