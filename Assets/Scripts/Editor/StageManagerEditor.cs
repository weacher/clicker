﻿using System.Collections;
using System.Collections.Generic;
using UnityEditor;
using UnityEditor.SceneManagement;
using UnityEngine;

[CustomEditor(typeof(StageManager))]
public class StageManagerEditor : Editor
{
    StageManager stageManager;

    public string nameTask;
    public string descriptionTask;
    public int form_id;
    public bool UniqueObj;
    public string condition_string;
    public Condition condition;

    public string nameStage;
    public string descriptionStage;


    public void OnEnable()
    {
        stageManager = (StageManager)target;
    }


    public override void OnInspectorGUI()
    {
        #region TaskPanel
        EditorGUILayout.BeginVertical("box");
        EditorGUILayout.BeginVertical("box");

        EditorGUILayout.BeginHorizontal();
        EditorGUILayout.LabelField("name task");
        nameTask = EditorGUILayout.TextField(nameTask);   
        EditorGUILayout.EndHorizontal();

        EditorGUILayout.BeginHorizontal();
        EditorGUILayout.LabelField("description task");
        descriptionTask = EditorGUILayout.TextField(descriptionTask);
        EditorGUILayout.EndHorizontal();

        EditorGUILayout.BeginHorizontal();
        EditorGUILayout.LabelField("id form");
        form_id = EditorGUILayout.IntField(form_id);
        EditorGUILayout.EndHorizontal();

        EditorGUILayout.BeginVertical("box");
        UniqueObj = EditorGUILayout.Toggle("uniqueObj",UniqueObj);
        EditorGUILayout.BeginHorizontal();
        EditorGUILayout.LabelField("condition type");
        condition_string = EditorGUILayout.TextField( condition_string);
        EditorGUILayout.EndHorizontal();
        EditorGUILayout.LabelField("Example: click/rotate");

        EditorGUILayout.EndVertical();
        EditorGUILayout.EndVertical();

        if (GUILayout.Button("Добавить таск", GUILayout.Height(40)))
        {
            switch (condition_string)
            {
                case "click":
                    condition = new Click(UniqueObj);
                    break;
                case "rotate":
                    condition = new Rotate(UniqueObj);
                    break;
                 default: condition = new Click(UniqueObj);
                    break;

            }
                stageManager.AddTask(nameTask, descriptionTask, form_id, condition);
        }
        EditorGUILayout.EndVertical();
        #endregion

        #region StagePanel
        EditorGUILayout.BeginVertical("box");
        EditorGUILayout.BeginVertical("box");

        EditorGUILayout.BeginHorizontal();
        EditorGUILayout.LabelField("name stage");
        nameStage = EditorGUILayout.TextField(nameStage);
        EditorGUILayout.EndHorizontal();

        EditorGUILayout.BeginHorizontal();
        EditorGUILayout.LabelField("description stage");
        descriptionStage = EditorGUILayout.TextField(descriptionStage);
        EditorGUILayout.EndHorizontal();
        EditorGUILayout.EndVertical();

        if (GUILayout.Button("Добавить Этап", GUILayout.Height(40)))
        {
            stageManager.AddStage(nameStage, descriptionStage);
        }
        EditorGUILayout.EndVertical();
        #endregion
    }

}
