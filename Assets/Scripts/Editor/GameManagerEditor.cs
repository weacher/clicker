﻿using System.Collections;
using System.Collections.Generic;
using UnityEditor;
using UnityEditor.SceneManagement;
using UnityEngine;

[CustomEditor(typeof(GameManager))]
public class GameManagerEditor : Editor
{
    GameManager gameManager;

    void OnEnable()
    {
        gameManager = (GameManager)target;
    }

    public override void OnInspectorGUI()
    {
        EditorGUILayout.BeginVertical("box");
        EditorGUILayout.BeginHorizontal();
        if (GUILayout.Button("Пройти таск", GUILayout.Height(40)))
        {
            gameManager.NextTask();
        }
        if (GUILayout.Button("Пройти Этап", GUILayout.Height(40)))
        {
            gameManager.NextStage();
        }
        EditorGUILayout.EndHorizontal();
        EditorGUILayout.EndVertical();
    }

}
