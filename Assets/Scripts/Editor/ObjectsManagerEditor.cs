﻿using System.Collections;
using System.Collections.Generic;
using UnityEditor;
using UnityEditor.SceneManagement;
using UnityEngine;

[CustomEditor(typeof(ObjectsManager))]
public class ObjectsManagerEditor : Editor
{
    ObjectsManager objectsManager;
    public bool IsClicked = false;
    public bool IsRotateted = false;

    public void OnEnable()
    {
        objectsManager = (ObjectsManager)target;
    }

    /// <summary>
    /// Не успел доделать добавление обьекта с заданными параметрами
    /// </summary>
    public override void OnInspectorGUI()
    {
        #region AddObject
        EditorGUILayout.BeginVertical("box");

        EditorGUILayout.LabelField("form type");
        IsClicked = EditorGUILayout.Toggle("IsClicked", IsClicked);
        IsRotateted = EditorGUILayout.Toggle("IsRotated", IsRotateted);
        EditorGUILayout.BeginHorizontal();

        if (GUILayout.Button("Добавить обьект", GUILayout.Height(40)))
        {
            objectsManager.AddObject(IsClicked, IsRotateted, new Vector3(Random.Range(-4f, 4f), Random.Range(-4f, 4f), Random.Range(-4f, 4f)));
        }

        if (GUILayout.Button("Добавить случайный обьект", GUILayout.Height(40), GUILayout.Width(240)))
        {
            objectsManager.AddObject();
        }

        EditorGUILayout.EndHorizontal();
        EditorGUILayout.EndVertical();
        #endregion

        #region ObjectList
        EditorGUILayout.BeginVertical("box");
      if (objectsManager.objects.Count > 0)
        {
            EditorGUILayout.LabelField("Objects List");
            for (int i= 0; i < objectsManager.objects.Count; i++)
            {
                EditorGUILayout.BeginVertical("box");
                EditorGUILayout.LabelField("id", objectsManager.objects[i].id.ToString());
                EditorGUILayout.LabelField("form type", objectsManager.objects[i].type.ToString());
                objectsManager.objects[i].IsClicked = EditorGUILayout.Toggle("IsClicked", objectsManager.objects[i].IsClicked);
                objectsManager.objects[i].IsRotateted = EditorGUILayout.Toggle("IsRotated", objectsManager.objects[i].IsRotateted);
                EditorGUILayout.EndVertical();
            }

        }
        else if(objectsManager.transform.childCount > 0)
        {
            EditorGUILayout.LabelField("Objects List");
            for (int i = 0; i < objectsManager.transform.childCount; i++)
            {
                EditorGUILayout.BeginVertical("box");
                EditorGUILayout.LabelField("id", objectsManager.transform.GetChild(i).GetComponent<Form>().id.ToString());
                EditorGUILayout.LabelField("form type", objectsManager.transform.GetChild(i).GetComponent<Form>().type.ToString());
                objectsManager.transform.GetChild(i).GetComponent<Form>().IsClicked = EditorGUILayout.Toggle("IsClicked", objectsManager.transform.GetChild(i).GetComponent<Form>().IsClicked);
                objectsManager.transform.GetChild(i).GetComponent<Form>().IsRotateted = EditorGUILayout.Toggle("IsRotated", objectsManager.transform.GetChild(i).GetComponent<Form>().IsRotateted);
                EditorGUILayout.EndVertical();
            }
        }
        EditorGUILayout.EndVertical();
        #endregion 
    }

}
