﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class MouseControl : MonoBehaviour
{
    public Camera camera;
    public ActivityManager activityManager;
    public ObjectsManager objectsManager;
    public Transform target;
    bool findFirstTarget = false;
    public float speedRotateX = 5;
    public float speedRotateY = 5;
    float rotateX;
    float rotateY;
    int RayLenght = 8;

    void Update()
    {
        RaycastHit hit;
        Ray ray = camera.ScreenPointToRay(Input.mousePosition);

        if (Input.GetMouseButton(1))
        {
            objectsManager.HighlightOffforAll();
            RaycastHit[] allHits = Physics.RaycastAll(ray, RayLenght);

            bool findtarget = false;
            for (int i = 0; i < allHits.Length; i++)
            {
                if (!findtarget && allHits[i].transform.gameObject.GetComponent<Form>() != null)
                {
                    target = allHits[i].transform;
                    findtarget = true;
                    if (!findFirstTarget)
                        findFirstTarget = true;
                }
            }

            if (findFirstTarget && target.GetComponent<Form>().IsRotateted && target.GetComponent<Form>().IsPropertiesActive) {

                rotateX = Input.GetAxis("Mouse X") * speedRotateX * Mathf.Deg2Rad;
                rotateY = Input.GetAxis("Mouse Y") * speedRotateY * Mathf.Deg2Rad;
                target.RotateAroundLocal(target.up, -rotateX);
                target.RotateAroundLocal(Camera.main.transform.right, rotateY);
                target.GetComponent<Form>().HighlightOn();
                return;
            }
        }

        if (Input.GetMouseButtonUp(1))
        {
            if (target.GetComponent<Form>().IsRotateted)
            {
                Vector3[] rotated = new Vector3[3] { new Vector3(-7.0f, 7.0f), new Vector3(-7.0f, 7.0f), new Vector3(-7.0f, 7.0f) };
                activityManager.addActivity(target.GetComponent<Form>(), activity_type.rotate, rotated);
                target.GetComponent<Form>().HighlightOff();
            }
        }
 
    }

   public void Reload()
   {
        findFirstTarget = false;
   }

}

