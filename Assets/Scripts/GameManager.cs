﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class GameManager : MonoBehaviour
{
    public ObjectsManager objectsManager;
    public StageManager stageManager;
    public ActivityManager activityManager;
    public UserErrorManager userErrorManager;
    public UIManager uIManager;
    public MouseControl mouseControl;

    public void Start()
    {
        StartCoroutine(Start_S());
    }


    public void Check(Activity activity)
    {
        if (stageManager.getActualTask().targetForm.type == activity.form.type
            && stageManager.getActualTask().condition.type.ToString() == activity.activityType.ToString())
        {
            var actualCondition = stageManager.getActualCondition();
            if (actualCondition.UniqueObj)
            {
                if (stageManager.getActualTask().targetForm.id == activity.form.id)
                {
                    switch (actualCondition.type)
                    {
                        case condition_type.click:
                            if ((actualCondition as Click).countClick == 1)
                            {
                                if ((activity as ClickActivity).activityType == activity_type.click)
                                    CompleteTask();
                                else
                                    userErrorManager.AddUserError(stageManager.indexStageActual + 1, stageManager.stages[stageManager.indexStageActual].indexActiveTask + 1, error_type.another_condition); 
                            }
                            break;
                        case condition_type.rotate:
                            if ((activity as RotateActivity).activityType == activity_type.rotate)
                                CompleteTask();
                            else
                                userErrorManager.AddUserError(stageManager.indexStageActual + 1, stageManager.stages[stageManager.indexStageActual].indexActiveTask + 1, error_type.another_condition); 
                            break;
                    }
                }
                else
                {
                    userErrorManager.AddUserError(stageManager.indexStageActual + 1, stageManager.stages[stageManager.indexStageActual].indexActiveTask + 1, error_type.another_obj);
                }
            }
            else
            {
                switch (actualCondition.type)
                {
                    case condition_type.click:
                        if ((actualCondition as Click).countClick == 1)
                        {
                            if ((activity as ClickActivity).activityType == activity_type.click)
                                CompleteTask();
                            else
                                userErrorManager.AddUserError(stageManager.indexStageActual + 1, stageManager.stages[stageManager.indexStageActual].indexActiveTask + 1, error_type.another_condition); 
                        }
                        break;
                    case condition_type.rotate:
                        if ((activity as RotateActivity).activityType == activity_type.rotate)
                            CompleteTask();
                        else
                            userErrorManager.AddUserError(stageManager.indexStageActual + 1, stageManager.stages[stageManager.indexStageActual].indexActiveTask + 1, error_type.another_condition); 
                        break;
                }
            }
        }
        else
        {
            userErrorManager.AddUserError(stageManager.indexStageActual + 1, stageManager.stages[stageManager.indexStageActual].indexActiveTask + 1, error_type.another_form); 
        }
    }


    public void CompleteAll()
    {
        uIManager.ShowFinalPanel(stageManager.stages.Count, userErrorManager.userErrors.Count);
    }


    public void FailStage()
    {
        objectsManager.Clear();
        uIManager.ShowFailPanel(userErrorManager.userErrors);
        activityManager.Clear();
        userErrorManager.ClearErrorHistory();
        stageManager.Clear();
    }


    public void Reload()
    {
        StartCoroutine(Start_S());
        activityManager.Clear();
        userErrorManager.ClearErrorHistory();
        stageManager.Reload();
        objectsManager.Reload();
        mouseControl.Reload();
    }


    public void UIUpdate()
    {
        uIManager.UpdateStagePanel(stageManager.stages[stageManager.indexStageActual]);
    }


    public void CompleteTask()
    {
        stageManager.CompleteTask();
        activityManager.Clear();
        uIManager.UpdateStagePanel(stageManager.stages[stageManager.indexStageActual]);
    }

    public void NextTask()
    {
        stageManager.CompleteTask();
        uIManager.UpdateStagePanel(stageManager.stages[stageManager.indexStageActual]);
    }


    public void NextStage()
    {
        stageManager.getActualTask().setComplete();
        stageManager.SetNextStage();
        uIManager.UpdateStagePanel(stageManager.stages[stageManager.indexStageActual]);
    }


    IEnumerator Start_S()
    {
        yield return new WaitForSeconds(0.1f);
        uIManager.UpdateStagePanel(stageManager.stages[stageManager.indexStageActual]);
        uIManager.ShowStagePanel();
    }


    public void CloseApp()
    {
        Application.Quit();
    }
}
