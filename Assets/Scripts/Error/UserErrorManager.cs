﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class UserErrorManager : MonoBehaviour
{
    public GameManager gameManager;
    public List<UserError> userErrors = new List<UserError>();

    public void AddUserError(int stageNumb, int taskNumb, error_type type)
    {
        userErrors.Add(new UserError(stageNumb, taskNumb, type));
        if(userErrors.Count >= 3)
        {
            gameManager.FailStage();
        }
    }
    
    public void ClearErrorHistory()
    {
        userErrors.Clear();
    }

    

}
