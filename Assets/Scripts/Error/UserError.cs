﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class UserError : MonoBehaviour
{
    public int stageNumb, taskNumb;
    public string description;
    public error_type type;

    public UserError(int stageNumb, int taskNumb, error_type type)
    {
        this.stageNumb = stageNumb;
        this.taskNumb = taskNumb;
        this.type = type;
    }

    public UserError(int stageNumb, int taskNumb, error_type type, string description)
    {
        this.stageNumb = stageNumb;
        this.taskNumb = taskNumb;
        this.type = type;
        this.description = description;
    }

}
