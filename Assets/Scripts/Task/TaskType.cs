﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class TaskType : MonoBehaviour
{
    public string name { get; set; }
    public string description { get; set; }
    public Form targetForm{ get; set; }
    public Condition condition { get; set; }

    public bool IsActive { get; set; }
    public bool IsComplete { get; set; }

    public TaskType(Form form, Condition condition)
    {
        this.targetForm = form;
        this.condition = condition;

        IsActive = false;
        IsComplete = false;
        
        name = "task";
        if (condition.UniqueObj)
            description = condition.type.ToString() + " " + form.type.ToString() + " id " + form.id.ToString();
        else
            description = condition.type.ToString() + " " + form.type.ToString(); 
    }


    public TaskType(string name, string description,Form form, Condition condition)
    {
        this.name = name;
        this.description = description;
        this.targetForm = form;
        this.condition = condition;

        IsActive = false;
        IsComplete = false;
    }


    public bool setActive()
    {
        if (!IsComplete)
        IsActive = true;

        return IsActive;
    }
    

    public void setComplete()
    {
        IsActive = false;
        IsComplete = true;
    }

}
