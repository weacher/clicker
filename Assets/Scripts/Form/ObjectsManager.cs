﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ObjectsManager : MonoBehaviour
{
    public GameManager gameManager;
    public List<Form> objects = new List<Form>();
    public ObjectsTemplateList templateList;
    public int generateMinCount = 3;
    public int generateMaxCount = 8;
    int objectsMaxCount = 20;

    void Start()
    {
        LoadObjects();
    }

    void GenerateObjects()
    {
        if ((generateMinCount > 0 && generateMaxCount > 0) && generateMaxCount > generateMinCount && generateMaxCount <= objectsMaxCount)
        for (int i = 0; i < Random.Range(3, 8); i++)
        {
            AddObject();
        }
    }


    void LoadObjects()
    {
        if (transform.childCount <= objectsMaxCount)
        {
            for (int i = 0; i < transform.childCount; i++)
            {
                objects.Add(transform.GetChild(i).GetComponent<Form>());
                objects[i].id = i;
            }
        }
    }


    public void AddObject(bool IsClicked, bool isRotated, Vector3 coordindate)
    {
        GameObject obj = Instantiate(templateList.templateObjects[Random.Range(0, templateList.templateObjects.Count)].gameObject, transform);
        obj.GetComponent<Form>().id = objects.Count;
        obj.GetComponent<Form>().IsClicked = IsClicked;
        obj.GetComponent<Form>().IsRotateted = isRotated;
        obj.transform.position = coordindate; 
        obj.GetComponent<MeshRenderer>().material = templateList.templateMaterials[Random.Range(0, templateList.templateMaterials.Count)];
        objects.Add(obj.GetComponent<Form>());
    }


    public void AddObject()
    {
        GameObject obj = Instantiate(templateList.templateObjects[Random.Range(0, templateList.templateObjects.Count)].gameObject, transform);
        obj.GetComponent<Form>().id =  objects.Count;
        obj.transform.position = new Vector3(Random.Range(-4f, 4f), Random.Range(-4f, 4f), Random.Range(-4f, 4f));
        obj.GetComponent<MeshRenderer>().material = templateList.templateMaterials[Random.Range(0, templateList.templateMaterials.Count)];
        objects.Add(obj.GetComponent<Form>());
    }


    public bool ObjectsContains(int id)
    {
        if (objects.Count > 0)
        {
            for (int i = 0; i < objects.Count; i++)
            {
                if (objects[i].id == id)
                    return true;
            }
        }
        else
        {
            for (int i = 0; i <transform.childCount; i++)
            {
                if (transform.GetChild(i).GetComponent<Form>().id == id)
                    return true;
            }
        }
        return false;

    }


    public Form GetObject(int id)
    {
        if (objects.Count > 0)
        {
            for (int i = 0; i < objects.Count; i++)
            {
                if (objects[i].id == id)
                    return objects[i];
            }
        }
        else
        {
            for (int i = 0; i < transform.childCount; i++)
            {
                if (transform.GetChild(i).GetComponent<Form>().id == id)
                    return transform.GetChild(i).GetComponent<Form>();
            }
        }
        return null;
    }


    public void HighlightOffforAll()
    {
        if (objects.Count > 0)
        {
            for (int i = 0; i < objects.Count; i++)
            {
                objects[i].HighlightOff();
            }
        }
    }


    public void DisActvePropertiesforAll()
    {
        if (objects.Count > 0)
        {
            for (int i = 0; i < objects.Count; i++)
            {
                objects[i].DisActiveProperties();
            }
        }
    }


    public void ActivePropertiesforAll()
    {
        if (objects.Count > 0)
        {
            for (int i = 0; i < objects.Count; i++)
            {
                objects[i].ActiveProperties();
            }
        }
    }


    public void Clear()
    {
        DisActvePropertiesforAll();
        HighlightOffforAll();
        objects.Clear();
    }


    public void Reload()
    {
        Clear();
        LoadObjects();
        ActivePropertiesforAll();
    }


}
