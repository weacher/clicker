﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ObjectsTemplateList : MonoBehaviour
{
    public List<Form> templateObjects = new List<Form>();
    public List<Material> templateMaterials = new List<Material>();
    public void Clear()
    {
        templateObjects.Clear();
    }

}
