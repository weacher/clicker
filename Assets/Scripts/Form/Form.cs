﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Form : MonoBehaviour
{
    [SerializeField]
    public form_type type;
    public int id;
    [SerializeField]
    public bool IsClicked;
    [SerializeField]
    public bool IsPropertiesActive = true;
    public bool IsRotateted;
    public bool IsHighlighted = false;   
    public ActivityManager activityManager;

    void OnMouseDown()
    {
        if (IsClicked && IsPropertiesActive)
        {
            Highlight();
            activityManager.addActivity(this, activity_type.click, new Vector3(1.1f, 2.2f, 3.1f));
        }
    }


    public Form(int id)
    {
        this.id = id;
        type = form_type.cube;
        IsClicked = true;
        IsRotateted = true;
        IsPropertiesActive = true;
    }


    public Form(int id, form_type type)
    {
        this.id = id;
        this.type = type;
        IsClicked = true;
        IsRotateted = true;
        IsPropertiesActive = true;
    }


    public Form(int id, form_type type, bool clicked, bool rotated)
    {
        this.id = id;
        type = form_type.cube;
        IsClicked = clicked;
        IsRotateted = rotated;
        IsPropertiesActive = true;
    }


    public void Highlight()
    {
        if (!IsHighlighted)
        {           
            transform.GetComponent<MeshRenderer>().material.EnableKeyword("_EMISSION");
            IsHighlighted = true;
        }
        else
        {
            transform.GetComponent<MeshRenderer>().material.DisableKeyword("_EMISSION");
            IsHighlighted = false;
        }
    }

    public void HighlightOn()
    {
        if (!IsHighlighted)
        {
            transform.GetComponent<MeshRenderer>().material.EnableKeyword("_EMISSION");
            IsHighlighted = true;
        }
    }

    public void HighlightOff()
    {
        if (IsHighlighted)
        {
            transform.GetComponent<MeshRenderer>().material.DisableKeyword("_EMISSION");
            IsHighlighted = false;
        }
    }


    public void DisActiveProperties()
    {
      IsPropertiesActive = false;  
    }


    public void ActiveProperties()
    {
        IsPropertiesActive = true ;
    }


}
