﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Rotate : Condition
{
    Vector3 vectorRotate { get; set; }

    public Rotate(float x, float y, float z)
    {
        description = "rotate";
        IsComplete = false;
        IsActive = false;
        type = condition_type.rotate;
        this.UniqueObj = false;

        if (x == 0 && y == 0 && z == 0)
        {
            x = 90;
        }

        vectorRotate = new Vector3(x, y, z);
    }

    public Rotate(Vector3 vector)
    {
        description = "rotate";
        IsComplete = false;
        IsActive = false;
        type = condition_type.rotate;

        if (vector.x == 0 && vector.y == 0 && vector.z == 0)
        {
            vector = new Vector3(90, 0, 0);
        }
        vectorRotate = vector;
    }

    public Rotate(Vector3 vector, bool UniqueObj)
    {
        description = "rotate";
        IsComplete = false;
        IsActive = false;
        type = condition_type.rotate;
        this.UniqueObj = UniqueObj;

        if (vector.x == 0 && vector.y == 0 && vector.z == 0)
        {
            vector = new Vector3(90, 0, 0);
        }
        vectorRotate = vector;
    }

    public Rotate(bool UniqueObj)
    {
        description = "rotate";
        IsComplete = false;
        IsActive = false;
        type = condition_type.rotate;
        this.UniqueObj = UniqueObj;

        vectorRotate = new Vector3(90, 0, 0);
    }

    public Rotate()
    {
        description = "rotate";
        IsComplete = false;
        IsActive = false;
        type = condition_type.rotate;
        this.UniqueObj = false;

        vectorRotate = new Vector3(90,0,0);
    }
}

