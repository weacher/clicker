﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public abstract class Condition : MonoBehaviour
{
    public condition_type type { get; set; }
    public string description { get; set; }
    public bool IsActive { get; set; }
    public bool IsComplete { get; set; }
    public bool UniqueObj { get; set; }

}
