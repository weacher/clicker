﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Click : Condition
{
    public int countClick { get; }

    public Click(int countClick)
    {
        description = "click"; 
        IsComplete = false;
        IsActive = false;
        type = condition_type.click;
        UniqueObj = false;

        if (countClick > 0)
            this.countClick = countClick;
        else
            this.countClick = 1;
    }

    public Click(int countClick, bool UniqueObj)
    {
        description = "click";
        IsComplete = false;
        IsActive = false;
        type = condition_type.click;
        this.UniqueObj = UniqueObj;

        if (countClick > 0)
            this.countClick = countClick;
        else
            this.countClick = 1;
    }

    public Click (bool UniqueObj)
    {
        description = "click";
        IsComplete = false;
        IsActive = false;
        type = condition_type.click;
        this.UniqueObj = UniqueObj;
        countClick = 1;
    }

    public Click()
    {
        description = "click";
        IsComplete = false;
        IsActive = false;
        type = condition_type.click;
        UniqueObj = false;
        countClick = 1;
    }


}

